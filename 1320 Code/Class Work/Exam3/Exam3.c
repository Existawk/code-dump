/* Parker Skinner
 *  ID: 1001541467
 *  5/5/2019
 *  Exam 3
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//problem 1
struct Node* reverse_list_rec(struct Node* head_ref)
{
  if (head_ref==NULL)
  {
    return;
  }
  if (head_ref->next==NULL)
  {
    return head_ref;
  }
  struct Node* head_rev=reverse_list_rec(head_ref->next);
  head_ref->next->next=head_ref;
  head_ref->next=NULL;
  return head_rev;
}
//problem 2
struct Node* age_sort(struct Node* line1, struct Node* line2)
{
  // assume the lines are already in age order
  if (line1==NULL&&line2==NULL)
  {
      return;
  }
  if (line1!=NULL&&line2==NULL)
  {
      return line1;
  }
  if (line1==NULL&&line2!=NULL)
  {
      return line2;
  }
  struct Node* merged = NULL;
  merged->head==NULL;

  while(line1!=NULL&&line2!=NULL)
  {
    if (line1->age>line2->age)
    {
      merged->age=line1->age;
      line1=line1->next;
    }
    else (line1->age<line2->age)
    {
      merged->age=line2->age;
      line2=line2->next;
    }
    merged=merged->next;
  }
  if(line1==NULL&&line2!=NULL)
  {
    merged->next=line2;
  }
  if(line2==NULL&&line1!=NULL)
  {
    merged->next=line1;
  }
  return merged;
}

//problem 3
int	check_exam(struct	node*	answer_key,	struct	node*	student_exam) {
  int compare = check_length(answer_key,student_exam);

  if(compare==2) {
    return 2;
  }
  compare = check_answers(answer_key,student_exam);

  if(compare==2) {
    return 2;
  }
  else {
    return 1;
  }
}

int check_length(struct node* answer_key, struct node* student_exam) {
  int counter_answer=0, counter_student=0;
  while(answer_key!=NULL) {
    answer_key = answer_key->next;
    counter_answer++;
  }
  while(student_exam!=NULL) {
    student_exam = student_exam->next;
    counter_student++;
  }
  if(counter_answer==counter_student) {
    return 1;
  }
  else {
    return 2;
  }
}

int check_answers(struct node* answer_key, struct node*student_exam) {
  while(answer_key!=NULL) {
    if(answer_key->answer[0]!=student_exam->answer[0]) {
      return 2;
    }
    answer_key=answer_key->next;
    student_exam=student_exam->next;
  }
  return 1;
}
//problem 4
Struct Node* odd(Struct Node* stack)
  Node* head, head_ref;
  head = head_ref = NULL;

  while(stack!=NULL) {
    if(stack->element % 2 == 1) {
      if(head_ref==NULL) {
        head_ref = stack;
      }
      head = stack;
      head = head->next;
    }
    stack = stack->next;
  }
  head->next = NULL
  return head_ref;
}
//problem 5
int *under_price(int queue[], int price)
{
  int i;
  int a;
  for(i=0;i<(sizeof(queue)/sizeof(queue[0])))
  {
    if(queue[i]<price)
    {
      a++;
    }
  }
  int stack[a];
  a=0;
  for(i=0;i<sizeof(queue))
  {
    if(queue[i]<price)
    {
      stack[a]=queue[i];
      a++;
    }
  return stack;
}
//problem 6
void change_case(Struct Node* tree)
{
  if(tree==NULL)
  {
    return;
  }

  if(tree->letter>96)
  {
    tree->letter-=32;
  }
  else
  {
    tree->letter+=32;
  }
  printf("%c\n", tree->letter);

  change_case(tree->left);
  change_case(tree->right);
}
