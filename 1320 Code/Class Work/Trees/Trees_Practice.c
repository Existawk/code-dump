/*
A type of data structure that resembles a "tree"

Parts:  Root
        terminal node
        Non terminal node (or a leaf)

Trees allow us to control data in a heirarchy

Binary Tree:  just a tree where every non-terminal node has 2 children

*/
BNODE* addNode(int number) {
  BNODE *temp=malloc(sizeof(BNODE));
  temp->number = number;
  temp->left = NULL;
  temp->right = NULL;

  return temp;
}

void preorder(BNODE *root) {
  if(root == NULL) {
    return;
  }
  printf("%d",root->number);
  preorder(root->left); //recursive calls of the preorder function
  preorder(root->right);
}

void inorder(BNODE *root) { // shows the numbers in order using recursion
  if(root == NULL) {
    return;
  }
  inorder(root->left);
  printf("%d", root->number);
  inorder(root->right);
}

void postorder(BNODE *root) { //these just print out the tree, the only difference is where the recursive call is
  if(root == NULL) {          // relative to the print function
    return;
  }
  postorder(root->left); //recursive calls of the postorder function
  postorder(root->right);
  printf("%d",root->number);
}

 void insert(BNODE *root, int number) {
   if(number<=root->number) {
     if (root->left==NULL) {
       root->left = addNode(number);
     }
     else {
       insert(root->left, number); //recursive call on function making root->left the parent or new "root"
     }
   }
   else {
     if (root->right == NULL) {
       root->right = addNode(number);
     }
     else {
       insert(root->right,number); //recursive call on function making root->right the parent or new "root"
     }
   }
 }
int main {
  BNODE *root
  int i;
  int d[]={1,2,3,4,5,6,7};
}
