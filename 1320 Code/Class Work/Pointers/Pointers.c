#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
  int apples=3;
  int *a_ptr=&apples;

  printf("apples=%d\n",apples);
  printf("addy of apples variable: %p", a_ptr);
  printf("The value at apples is: %d", *a_ptr);

  char letter='f';
  char *c_ptr=&letter;

  float price=4.50;
  float *p_ptr=&price;

  //arrays
  char words[]="cat";

  printf("words array:%p\n", words); //this prints out the adress of the char array

  char *ptr1=&words[0];
  char *ptr2=&words[1];
  char *ptr3=&words[2];

  printf("%p, %p, %p", ptr1, ptr2, ptr3 );
  printf("%, %, %", *ptr1, *ptr2, *ptr3 );

  
}
