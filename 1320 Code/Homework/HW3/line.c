/* Parker Skinner
*  ID: 1001541467
*  2/19/2019
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int	find_filename(int n,char **b)
{
  int	i;
  int	counter=0; /*equals -1 if didnt find*/
  int	check=0;
    for(i=0;i<n&&check==0;i++)
    {
      if(strcmp(*b,	"filename")==0)
        {
          check=1;
        }
      counter++;
      b++;
    }
    if(check!=1)
    {
      counter=0;
    }
    return (counter-1);
}

int main(int argc, char **argv)
{
  int check=find_filename(argc,argv);
  if (check==-1)
  {
    printf("No filename given. Bye!\n");
    exit(0);
  }

  printf("Filename: %s\n",argv[(check+1)]);
  printf("We're dealing with %s info\n",argv[(check+2)]);

  FILE *fp;
  char *mode="r";
  char line[100];
  fp=fopen(argv[(check+1)],mode);

  if(fp==NULL)
  {
    printf("No file found\n");
  }
  else
  {
    printf("\nContents of the file:\n");
    while(fgets(line,100,fp)!=NULL)
    {
      printf("%s",line);
    }
  }
  fclose(fp);
}
