/* Parker Skinner
*  ID: 1001541467
*  2/19/2019
*/
#include <string.h>
#include <stdio.h>

int main(int argc,char **argv)
{
  int siblings=3;
  int totalcandy,tempcandy,mariacandy;
  int i;

  printf("How much candy is there total?\n");
  scanf("%d",&totalcandy);

  for(i=0;i<siblings;i++)
  {
    printf("How much candy does sibling %d get?\n",(i+1));
    scanf("%d",&tempcandy);
    totalcandy-=tempcandy;
  }

  if(totalcandy>0)
  {
    printf("Maria gets %d pieces of candy\n",totalcandy );
  }
  else
  {
    printf("There is no candy left for maria :(\n");
  }
}
