/* Parker Skinner
*  ID: 1001541467
*  2/19/2019
*/
#include <string.h>
#include <stdio.h>
#include <ctype.h>

void print_out(int *num,char* f[])
{
  int i;
  printf("\n%d Flavors:\n",*num);
  for(i=0;i<*num;i++)
  {
    printf("%s\n",f[i]);
  }
}

int name_flavors(char *n,char* f[],int *s)
{
  int matches=0;
  int i;

  for(i=0;i<*s;i++)
  {
    if((*n)==*f[i])
    {
      printf("Flavor match! %s\n",f[i]);
      matches++;
    }
  }
  return matches;
}

int main(int argc, char **argv)
{
  char* flavors[]={"Chocolate Chocolate Chip","Clasic Vanilla","Red Velvet","White Chocolate Raspberry","Confetti","Carrot","Lemon","Marble","Pecan Praline"};
  int check=1;
  char name[30];
  int matches;
  int flavorlistsize=(sizeof(flavors)/sizeof(flavors[0]));
  while(check)
  {
    int input;
    printf("\n***Menu:***\n1-pick how many you want\n2-see if any match the first letter of your name\n3-to eixt\n");
    scanf("%d",&input);

    if(input==1)
    {
      int num;
      printf("\nHow many do you want?\n");
      scanf("%d",&num);
      while(num>(flavorlistsize))
      {
        printf("\nYour number is too big! input a smaller number:\n");
        scanf("%d",&num);
      }
      print_out(&num,flavors);
    }
    else if (input==2)
    {
      printf("\nEnter a name:\n");
      scanf("%s",&name);
      char firstletter=name[0];
      firstletter=toupper(firstletter);
      matches=name_flavors(&firstletter,flavors,&flavorlistsize);
      printf("Number of matches: %d\n",matches);
    }
    else if (input==3)
    {
      printf("Bye!\n");
      check=0;
    }
    else
    {
      printf("Not a valid input, Try again.\n");
    }
  }
}
