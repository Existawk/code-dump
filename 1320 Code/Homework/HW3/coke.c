/* Parker Skinner
*  ID: 1001541467
*  2/19/2019
*/
#include <string.h>
#include <stdio.h>

int main(int argc, char **argv)
{
  int CokesPerCrate,CokesPerTruck,Cokes,Trash;

  printf("How many cokes are there in a truck?\n");
  scanf("%d",&CokesPerTruck);
  CokesPerCrate=CokesPerTruck/10;
  printf("How many cokes are there total?\n");
  scanf("%d",&Cokes);
  Trash=(Cokes%CokesPerCrate);
  printf("%d cans of coke are being trashed\n",Trash);
}
