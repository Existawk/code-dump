/* Parker Skinner
*  ID: 1001541467
*  1/28/2019
*/
#include <stdio.h>

int age_looper(int patients,int MedicareAge)
{
  int num=0;
  int i;
  for(i=0;i<patients;i++)
  {
    int age=0;
    printf("Enter age of patient %d: ", i+1);
    scanf("%d", &age);
    if(age>=MedicareAge)
    {
      num++;
    }
  }
  return num;
}

int main(int argc, char **argv[])
{
  int MedicareAge=65;
  int hos1;
  int hos2;

  printf("Number of patients in hospital 1: ");
  scanf("%d", &hos1);
  int eligable1=age_looper(hos1,MedicareAge);

  printf("Number of patients in hospital 2: ");
  scanf("%d", &hos2);
  int eligable2=age_looper(hos2,MedicareAge);

  if(eligable1<eligable2)
  {
    printf("You Should pick hospital 2 - they have more Medicare patients\n");
  }
  else if(eligable2<eligable1)
  {
    printf("You Should pick hospital 1 - they have more Medicare patients\n");
  }
  else
  {
    printf("they have the same number of Medicare patients - go for either\n");
  }
}
