/* Parker Skinner
*  ID: 1001541467
*  2/6/2019
*/
#include <stdio.h>
#include <stdlib.h>

float print_out(float *salary, int *num_of_emps)
{
  printf("\n");
  if(*num_of_emps<6)
  {
    printf("Not enough employees!\n");
    return 0;
  }

  int managers=*num_of_emps/2;
  int directors=managers/3;
  float directorsalary=*salary*5;
  float managersalary=directorsalary/2;
  float total=(*salary*(*num_of_emps))+(directors*directorsalary)+(managers*managersalary);

  printf("\n**Employee info:**\n");
  printf("Total workers: %d    Monthly salary: $%0.2f\n",*num_of_emps, *salary);
  printf("Total managers: %d    Monthly salary: $%0.2f\n", managers, managersalary);
  printf("Total directors: %d   Monthly salary: $%0.2f\n", directors, directorsalary);
  printf("Total spent: $%0.2f\n",total );
  return total;
}

int rebudget(float *d, float *budget)
{
  int rebudget=0;
  if (*d==0)
  {
    return 1;
  }
  printf("--------\n");
  if(*budget-*d>0)
  {
    printf("--This goes UNDER your budget by $%0.2f.\n",*budget-*d);
  }
  else
  {
    printf("--This goes OVER your budget by $%0.2f.\n",(*budget-*d)*-1);
  }

  printf("\nWould you like to rebudget? 1 for yes 2 for no.\n");
  scanf("%d",&rebudget);
  if(rebudget==1)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

int	main(int	argc,	char	**	argv)
{
  int	i=1;
  float	budget,	total, salary;
  float	*money_ptr=&salary;
  float	*total_ptr=&total;
  int	employees;
  printf("Enter monthly budget:$");
  scanf("%f",	&budget);

  while(i)
    {
      printf("Enter monthly worker salary:$");
      scanf("%f",	&salary);
      printf("Enter total workers:");
      scanf("%d", &employees);
      total=print_out(money_ptr, &employees);
      i=rebudget(total_ptr, &budget);
    }
}
