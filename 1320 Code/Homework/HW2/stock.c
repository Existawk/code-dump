/* Parker Skinner
*  ID: 1001541467
*  2/6/2019
*/
#include <stdio.h>

int main(int argc, char **argv[])
{
  int MoneyStart=0;
  int MoneyNow=0;

  printf("How much money did you start off with? ");
  scanf("%d", &MoneyStart);

  printf("How much money do you have now? ");
  scanf("%d", &MoneyNow);

  int MoneyTotal=MoneyStart-MoneyNow;

  if(MoneyTotal>0)
  {
    printf("You lost %d dollars\n",MoneyTotal);
  }
  else if(MoneyTotal<0)
  {
    printf("You gained %d dollars\n",MoneyTotal*-1);
  }
  else
  {
    printf("You broke even\n" );
  }
}
