/* Parker Skinner
*  ID: 1001541467
*  2/6/2019
*/
#include <stdio.h>

int main(int argc, char **argv[])
{
  int CurrentUmbrellas=0;
  int ShipmentUmbrellas=0;

  printf("How many umbrellas do you currently have?\n");
  scanf("%d", &CurrentUmbrellas);

  printf("How many umbrellas are in the shipment?\n");
  scanf("%d", &ShipmentUmbrellas);

  printf("You have a total of %d Umbrellas!\n", CurrentUmbrellas+ShipmentUmbrellas);
}
