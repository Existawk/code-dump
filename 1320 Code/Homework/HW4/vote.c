/* Parker Skinner
*  ID: 1001541467
*  3/5/2019
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int registered(FILE *fp);
void new_register(FILE *fp);

int main(int argc, char **argv)
{
  int total_registered;
  int people;
  char response[1];
  int i=0;

  FILE *fp;
  fp=fopen(argv[1],"a+");

  if(fp==NULL)
  {
    printf("File not found, exiting.....\n");
    exit(0);
  }

  total_registered=registered(fp);
  if(total_registered>=10)
  {
    printf("Target Reached! exiting...\n");
    exit(0);
  }

  printf("\n\nHow many people to ask right now?\n");
  scanf("%d",&people);
  int num=people;

  while((i<num) && ((total_registered)<10))
  {
    printf("\n-Person %d: Would you like to register to vote?\n",i+1);
    scanf("%s", response);

    if(strcmp(response,"y")==0)
    {
      new_register(fp);
      total_registered=registered(fp);
    }
    else
    {
      printf("Ok\n\n");
    }
    i++;
  }
  if((total_registered)>=10)
  {
    printf("Target Reached! exiting...\n");
    exit(0);
  }
  printf("%d people asked! Taking a break.\n",num);
  fclose(fp);
}

int registered(FILE *fp)
{
    int counter=0;
    char line[100];
    printf("***Registered so far:***\n\n");
    while(fgets(line,100,fp)!=NULL)
    {
      printf("%s",line);
      counter++;
    }
    return counter;
}

void new_register(FILE *fp)
{
  char line[40];
  char name;
  printf("Enter Name: ");
  scanf("%c", &name);
  fgets(line,40,stdin);
  printf("Adding: %s\n",line);
  fprintf(fp,"%s",line);
}
