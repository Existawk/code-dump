/* Parker Skinner
*  ID: 1001541467
*  3/5/2019
*/
#include <stdio.h>
#include <string.h>
#include <math.h>

int main(int argc, char **argv)
{
  int pop1;
  int pop2;
  int years;
  float rate;
  printf("What was the population of town ABC?\n");
  scanf("%d",&pop1);
  printf("How many years have passed since then?\n");
  scanf("%d",&years);
  printf("What is the growth rate over this time period?\n");
  scanf("%f",&rate);
  pop2=pop1*exp(rate*years);
  printf("Over the past %d years, the poplutaion has grown from %d to %d\n",years,pop1,pop2);
}
