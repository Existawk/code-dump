/* Parker Skinner
*  ID: 1001541467
*  3/5/2019
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct dragon
{
    char name[20];
    char colors[3];
    int heads;
    int tails;
}dragon;

void dragon_info(dragon *d)
{
  char line[200];
  fgets(line,200,stdin);
  char *token;
  token=strtok(line,",");
  int i;
  char a,b,c;
  for(i=0;i<4;i++)
  {
    token=strtok(NULL,",");
    strcpy(d[i].name,token);
    token=strtok(NULL,",");
    a=*token;
    token=strtok(NULL,",");
    b=*token;
    token=strtok(NULL,",");
    c=*token;
    *d[i].colors=({a,b,c;});
    strtok(NULL,",");
    d[i].heads=(atoi(token));
    strtok(NULL,",");
    d[i].tails=(atoi(token));
  }
}

void color(char *color,dragon *d)
{
  int i;
  int j;
  char *c="";
  if(color[0]=='r')
  {
    c="Red";
  }
  if(color[0]=='b')==0)
  {
    c="Blue";
  }
  if(color[0]=='w')==0)
  {
    c="White";
  }
  if(color[0]=='g')==0)
  {
    c="Green";
  }
  if(color[0]=='y')==0)
  {
    c="Yellow";
  }
  for(i=0;i<sizeof(d);i++)
  {
    for(j=0;j<sizeof(d[i].colors);j++)
    {
      if(d[i].colors[j]==color[0])
      {
        printf("%s is %s\n",d[i].name, c);
      }
    }
  }
}

int main(int argc, char **argv)
{
  dragon total[4];
  dragon_info(total);
  char c='b';
  color(&c,total);
}
