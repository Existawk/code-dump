/* Parker Skinner
*  ID: 1001541467
*  3/5/2019
*/
#include <stdio.h>
#include <string.h>
#include <math.h>

int main(int argc, char **argv)
{
  int flu=32;
  int year=0;
  printf("This year there has been %d flu cases\n",flu);
  while(flu>=2)
  {
    flu=flu/2;
    year++;
  }
  printf("This flu outbreak has been going on for %d years\n", year);
}
