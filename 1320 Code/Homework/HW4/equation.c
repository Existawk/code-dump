/* Parker Skinner
*  ID: 1001541467
*  3/5/2019
*/
#include <stdio.h>
#include <string.h>
#include <math.h>\

int main(int argc, char **argv)
{
  float pi=3.14159265359;
  float theta;
  float theta_r;
  float Sin1;
  float Cos1;
  printf("what is your theta in degrees?: \n");
  scanf("%f", &theta);
  theta_r=(theta/180)*pi;
  Sin1=sin((pi/2)-theta_r);
  Cos1=cos(theta_r);
  printf("Sin is %0.2f and Cos is %0.2f\n",Sin1,Cos1);
}
