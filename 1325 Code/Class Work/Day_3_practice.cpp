#include <iostream>
#include <vector>
#include <string>

using namespace std

namespace mars{

  string alien_name="ET";

  void who_is_present();
  {
    cout<<"*** "<<alien_name<<" is currently on mars. ***"<<endl;
  }

  void change_name()
  {
      string answer;

      cout<<"Enter new name?"<<endl;

      cin>>answer;

      if(answer.compare("yes")==0)
      {
        cout<<"New Name:"<<endl;
        cin>>alien_name;
      }
  }
}

namespace jupiter{

  string alien_name="Marvin";

  void who_is_present();
  {
    cout<<"*** "<<alien_name<<" is currently on jupiter. ***"<<endl;
  }

  void change_name()
  {
      string answer;

      cout<<"Enter new name?"<<endl;

      cin>>answer;

      if(answer.compare("yes")==0)
      {
        cout<<"New Name:"<<endl;
        cin>>alien_name;
      }
  }
}

int which_planet(string answer)
{
  int ret;

  if(answer.compare("jupiter")==0)
  {
     ret=0;
  }
  if(answer.compare("mars")==0)
  {
     ret=1;
  }
  if(answer.compare("exit")==0)
  {
     ret=2;
  }
  else
  {
     ret=3;
  }
}

using namespace mars;

int main(int argc, char ** argv)
{
  string answer;
  int choice;

  cout<<"which planet are you interested it?"<<endl;
  cin>>answer;

  choice=which_planet(answer);

  if(choice==2)
  {
    cout<<"Bye!"<<endl;
  }

  else if(choice==3)
  {
    cout<<"Unknown Input"<<endl;
  }

  else if(choice==1)
  {
    cout<<"mars info"<<endl;
    who_is_present();

    cout<<"change name?"<<endl;
    cin>>answer;

    if(answer=="yes")
    {
      change_name();
    }
  }

  else if(choice==2)
  {
    cout<<"jupiter info"<<endl;
    jupiter::who_is_present();

    cout<<"change name?"<<endl;
    cin>>answer;

    if(answer=="yes")
    {
      jupiter::change_name();
    }
}
