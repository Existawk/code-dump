/* Parker Skinner
*  ID: 1001541467
*  9/4/2019
*/

#include <iostream>
#include <string>

using namespace std;

int ShapeCeck(string input, string shape[][])
{
  for(int i=0;i<shape.length();i++)
  {
    if(shape[i][0].compare(input)==0)
    {
      return 0;
    }
    if(shape[i][0].compare(input)==0)
    {
      return 1;
    }
    if(shape[i][0].compare(input)==0)
    {
      return 2;
    }
  }
  return -1;
}

int main(int argc, char **argv)
{
  string shapeinfo [][]= {{"Square","area=(base)^2\nperimeter=4*(base)"}
                         {"Rectangle","area=(base)*(height)\nperimeter=2*(base)+2*(height)"}
                         {"Triangle","area=((base)*(height))/2\nperimeter=sqrt((base)^2+(height)^2)"}};
  string input = " ";
  int i=1;
  int j=0;
  cout << "|_| S H A P E S |_|\n\n" << endl;

  while (input.compare("eixt")!=0)
  {
    cout << i << ". Shape: " << endl;
    cin >> input;

    int index=ShapeCeck(input);

    if(index!=-1)
    {
      cout << shapeinfo[index] << endl;
      j++;
    }

    else
    {
      cout << "Sorry, information not found on that shape." << endl;
    }

    i++;
  }

  cout << "Exiting..." << cin;
}
