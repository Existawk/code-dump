/* Parker Skinner
*  ID: 1001541467
*  9/4/2019
*/
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

bool info_check(string a, int b, string c)
{
  if(a.compare("")==0 || b==-1 || c.compare("")==0)
  {
    return false;
  }
  else
  {
    return true;
  }
}

void conversion(string name, int weight, string unit)
{
  if (unit.compare("kilos")==0)
  {
    weight=weight*2.20462;
    cout << "Hi " << name << " you weigh " << weight << " pounds." << endl;
  }

  else if (unit.compare("pounds")==0)
  {
    weight=weight*0.453592;
    cout << "Hi " << name << " you weigh " << weight << " kilos." << endl;
  }

  else
  {
    cout << "Improper Input\n" << endl;
  }
}

int main(int argc, char **argv)
{
  string input;
  string name;
  int weight;
  string unit;

  while (name.compare("exit")!=0)
  {
    name="";
    weight=0;
    unit="";

    cout << "Please enter your name and weight." << endl;
    getline(cin, input);
    stringstream ss(input);
    ss >> name >> weight >> unit;

    if(info_check(name, weight, unit))
    {
      conversion(name, weight, unit);
    }

    else
    {
      cout << "Not enough info to convert" << endl;
    }
  }

  cout << "Exiting..." << endl;

}
