////////////////////////////////////////////
///   Seth Jaksik
///   ID: 1001541359
///   chili.cpp
///
///   Purpose: Allow users to read in customer information
///   then serve them chili based on their cup size
////////////////////////////////////////////

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <fstream>
#include <sstream>
#include <map>

class chili
{
public:
    float amountChili;

    chili(int batches)
    {
        amountChili = float(batches * 4);
    }
    int serveChili(float amount)
    {
        if (amount > amountChili)
        {
            return -1;
        }
        else
        {
            amountChili = amountChili - amount;
            if (amountChili < 2)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }
    }

    void addChili(float amount)
    {
        amountChili += amount * 4;
    }
};

class louise
{
public:
    chili *chiliPot;
    std::vector<std::string> customers;
    std::vector<std::string> orders;
    louise(char *filename)
    {
        std::cout << "Checking today's customers...";
        readInCustomers(filename);
        std::cout << "done!\n"
                  << std::endl;
    }
    ~louise()
    {
        delete chiliPot;
    }
    void readInCustomers(char* filename)
    {
        std::string input, intermediate, name, order;
        std::ifstream inFile(filename);
        if (!inFile.is_open())
        {
            std::cout << "File did not open correctly. Aborting..." << std::endl;
            exit(0);
        }
        while (!inFile.eof())
        {
            std::getline(inFile, input);
            std::stringstream delim(input);
            std::getline(delim, name, ',');
            customers.push_back(name);
            std::getline(delim, order, '\n');
            orders.push_back(order);
        }
        inFile.close();
    }
    void serveChili(std::string ammount)
    {
        float ammountChili = 0;
        int result = 0;
        if (ammount.compare("Short") == 0)
        {
            ammountChili = 1;
        }
        else if (ammount.compare("Tall") == 0)
        {
            ammountChili = 1.5;
        }
        else if (ammount.compare("Grande") == 0)
        {
            ammountChili = 2;
        }
        else if (ammount.compare("Venti") == 0)
        {
            ammountChili = 2.5;
        }
        result = chiliPot->serveChili(ammountChili);
        switch (result)
        {
        case -1:
        {
            std::cout << "Sorry, not enough chili.  Would you like to make another batch or quit?" << std::endl;
            std::getline(std::cin, ammount);
            if (ammount.compare("make") == 0)
            {
                std::cout << "\nHow many more batches do you want to make: ";
                std::getline(std::cin, ammount);
                chiliPot->addChili(stof(ammount));
                std::cout << "\n";
            }
            else
            {
                std::cout << "Quiting..." << std::endl;
                exit(0);
            }
        }
        break;
        case 1:
        {
            std::cout << "Order served. Not much chili left!\n\n"
                      << std::endl;
        }
        break;
        case 2:
        {
            std::cout << "Order served. Still got lots of chili!\n\n"
                      << std::endl;
        }
        break;
        }
    }
    void set_chiliPot(chili *pot)
    {
        this->chiliPot = pot;
    }
};

int main(int argc, char **argv)
{
    int numBatches = 0, counter = 0;
    std::string input;
    std::cout << "\n--Welcome Louise--" << std::endl;
    louise louiseCompany(argv[1]);
    std::cout << "How many batches of your famuos chili are you making today? ";
    std::getline(std::cin, input);
    numBatches = stoi(input);
    chili *potPtr = new chili(numBatches);
    louiseCompany.set_chiliPot(potPtr);

    std::cout << "\nStarting orders...\n"
              << std::endl;
    for (int i = 0; i < louiseCompany.customers.size(); i++)
    {
        std::cout << "--Customer " << (counter + 1) << ": " << louiseCompany.customers.at(i) << "'s order is " << louiseCompany.orders.at(i) << "." << std::endl;
        louiseCompany.serveChili(louiseCompany.orders.at(i));
        std::cout << "Continue with orders or take a break?" << std::endl;
        std::getline(std::cin, input);
        std::cout << "\n";
        if (input.compare("break") == 0)
        {
            std::cout << "Ok.  Press enter to continue when you are finished with your break." << std::endl;
            std::getline(std::cin, input);
            std::cout << "\n";
        }
        counter++;
    }
    std::cout<<"No more customers... Exiting...\n";
}