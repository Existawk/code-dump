////////////////////////////////////////////
///   Seth Jaksik
///   ID: 1001541359
///   chipotle.cpp
///
///    Purpose: Allow the user to either order or
///    apply to work at chipotle, then after all orders
///    have been filled or the user quits, print total made
////////////////////////////////////////////

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>
class order
{
private:
    float orderCost;
    std::string thingsOrdered;
    std::string deliveryPerson;

public:
    order(float orderCost, std::string order, std::string deliveryPerson)
    {
        this->orderCost = (orderCost * 1.20); // 20% delivery fee
        this->thingsOrdered = thingsOrdered;
        this->deliveryPerson = deliveryPerson;

        std::cout << std::fixed << std::setprecision(2) << "Okay, " << deliveryPerson << " will be delivering your order for a total of " << this->orderCost << ". Thanks!" << std::endl;
    }
    float get_orderCost()
    {
        return orderCost;
    }
    std::string get_thingsOrdered()
    {
        return thingsOrdered;
    }
    std::string get_deliveryPerson()
    {
        return thingsOrdered;
    }
};

class DeliveryService
{
private:
    std::vector<std::string> all_deliveryPerson;
    std::vector<order> all_orders;
    int numDelivery;

public:
    DeliveryService(char *filename)
    {
        std::string input;
        std::ifstream inFile(filename);
        numDelivery = 0;
        if (!inFile.is_open())
        {
            std::cout << "File not opened successfully, aborting..." << std::endl;
            exit(0);
        }
        while (!inFile.eof())
        {
            std::getline(inFile, input);
            input = input.substr(0, input.find(" "));
            all_deliveryPerson.push_back(input);
            numDelivery++;
        }
        inFile.close();
    }
    void placeOrder()
    {
        std::string input;
        std::string totalOrder;
        float price = 0.0;
        std::cout << "***Place your order***\nBurrito or Bowl?" << std::endl;
        std::getline(std::cin, input);
        if (input.compare("burrito") == 0)
        {
            std::cout << "Price will be $6.75" << std::endl;
            price = 6.75;
            totalOrder = "Burrito: ";
        }
        else if (input.compare("bowl") == 0)
        {
            std::cout << "Price will be $7.25" << std::endl;
            price = 7.25;
            totalOrder = "Bowl: ";
        }
        else
        {
            std::cout << "Not a valid order type..." << std::endl;
            return;
        }
        std::cout << "\n";
        std::cout << "-Pick: Tofu, Steak, Chicken" << std::endl;
        std::getline(std::cin, input);
        if (input.compare("none") != 0)
        {
            totalOrder += input;
        }
        std::cout << "\n";
        std::cout << "-Pick: Cilantro-Lime Brown, Cilantro-Lime White" << std::endl;
        std::getline(std::cin, input);
        if (input.compare("none") != 0)
        {
            totalOrder += (", " + input);
        }
        std::cout << "\n";
        std::cout << "-Pick:  Queso, Sour Cream, Fresh Tomato Salsa" << std::endl;
        std::getline(std::cin, input);
        if (input.compare("none") != 0)
        {
            totalOrder += (", " + input);
        }
        std::cout << "\n";
        std::cout << "Confirm order (Yes or No):\n"
                  << totalOrder << std::endl;
        std::getline(std::cin, input);
        if (input.compare("Yes") == 0 && all_deliveryPerson.size() != 0)
        {
            order temp(price, totalOrder, all_deliveryPerson.at(numDelivery - 1));
            numDelivery--;
            all_deliveryPerson.pop_back();
            all_orders.push_back(temp);
        }
        else
        {
            std::cout << "Unable to process order. Either you said no or there are not enough delivery people." << std::endl;
        }
    }
    void addDeliverPerson()
    {
        std::string input;
        std::cout << "Enter your full name: ";
        std::getline(std::cin, input);
        input = input.substr(0, input.find(" "));
        std::cout << "Newest delivery person: " << input << std::endl;
        all_deliveryPerson.push_back(input);
        numDelivery++;
    }
    void sumOrders()
    {
        float totalMade = 0;
        std::ofstream outFile("totalCost.txt");
        for (int i = 0; i < all_orders.size(); i++)
        {
            totalMade += all_orders.at(i).get_orderCost();
        }
        std::cout << "Total made: $" << totalMade << std::endl;
        outFile << std::fixed << std::setprecision(2) << "Total made: $" << totalMade << std::endl;
        outFile.close();
    }
};

int main(int argc, char **argv)
{
    DeliveryService restraunt(argv[1]);
    std::string choice;
    std::cout << "~~Ronnie's Delivery Service~~" << std::endl;

    while (true)
    {
        std::cout << "\n--------------------------------------" << std::endl;
        std::cout << "Pick from the following menu:\n1. Customer\n2. Apply\n3. Exit" << std::endl;
        std::getline(std::cin, choice);
        std::cout << "\n";

        switch (stoi(choice))
        {
        case 1:
        {
            restraunt.placeOrder();
        }
        break;
        case 2:
        {
            restraunt.addDeliverPerson();
        }
        break;
        case 3:
        {
            std::cout << "Exiting..." << std::endl;
            restraunt.sumOrders();
            exit(0);
        }
        break;
        default:
        {
            std::cout << "Not a valid choice. Please pick a valid choice again." << std::endl;
        }
        break;
        }
    }
}