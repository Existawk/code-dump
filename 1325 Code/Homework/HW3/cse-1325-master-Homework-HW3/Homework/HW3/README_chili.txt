***README***
--------------------------------------------------------------
Maintainer: Seth Jaksik seth.jaksik@mavs.uta.edu
            ID: 1001541359

Files Included: chili.cpp
--------------------------------------------------------------

chili.cpp
--------------------------------------------------------------
Compilation Instructions:
g++ chili.cpp -o chili

Run Commands:
./chili inputfile.txt

Notes:
    - The command to make more chili when prompted is "make"
    - Each batch of chili is 4 cups
    - The size cups available are:
        Short - 1 cup
        Tall - 1.5 cups
        Grande - 2 cups
        Venti - 2.5 cups
    - The inputfile.txt should be the file of customers and their cup sizes.

Sample Runs:

./a.out chili.txt

--Welcome Louise--
Checking today's customers...done!

How many batches of your famous chili are you making today? 1

Starting orders...

--Customer 1: Natassa's order is Grande.
Order served. Still got lots of chili!

continue with orders or take a break?
continue

--Customer 1: Natassa's order is Grande.
Order served. Not much chili left!

continue with orders or take a break?
continue

--Customer 1: Natassa's order is Grande.
Sorry, not enough chili. Would you like to make another batch or quit?
make

How many more batches do you want to make: 1

Continue with orders or take a break?
continue

No more customers... Exiting...