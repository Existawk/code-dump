***README***
--------------------------------------------------------------
Maintainer: Seth Jaksik seth.jaksik@mavs.uta.edu
            ID: 1001541359

Files Included: chipotle.cpp
--------------------------------------------------------------

chipotle.cpp
--------------------------------------------------------------
Compilation Instructions:
g++ chipotle.cpp -o chipotle

Run Commands:
./chipotle inputfile.txt

Notes:
    - you can make either burritos or bowls, input as "burrito" or "bowl"
    - the topping options are listed in the program
    - the output file of the program is totalCost.txt

Sample Runs:

~~Ronnie's Delivery Service~~

--------------------------------------
Pick from the following menu:
1.Customer
2.Apply
3.Exit
1

***Place your order***
Burrito or Bowl?
burrito
Price will be $6.75

-Pick: Tofu, Steak, Chicken
Tofu, Chicken

-Pick: Cilantro-Lime Brown, Cilantro-Lime White
none

-Pick:  Queso, Sour Cream, Fresh Tomato Salsa
Queso

Confirm order (yes or no):
Burrito: Tofu, Chicken, Queso
Yes

Okay, John will be delivering your order for a total of 8.10. Thanks!

--------------------------------------
Pick from the following menu:
1.Customer
2.Apply
3.Exit
2

Enter your full name: Seth Jaksik
Newest delivery person: Seth

--------------------------------------
Pick from the following menu:
1.Customer
2.Apply
3.Exit
3

Exiting...
Total made: $8.10