#include <iostream>
using namespace std;
class Food
{
protected:
    float price;

public:
    void give_info(float price) { this->price = price; }
    float get_price() { return price; }
    void change_price(float a) { price = a; }
};
class Watermelon : public Food
{
    bool seedless;

public:
    void set_seed(int a)
    {
        if (a < 3)
        {
            seedless = true;
        }
        else
        {
            seedless = false;
        }
    }
    bool get_seedless() { return seedless; }
};
class Apple : protected Food
{
};
class Grape : private Food
{
    string color;

public:
    int number;
};
class Employee
{
public:
    void discount(Food &f1)
    {
        float amount;
        cout << "How much of a discount? (Enter in decimal form)" << endl;
        cin >> amount;
        f1.change_price(f1.get_price() * (1 - amount));
    } //true=both, false=not both
    bool check_watermelon(Watermelon w1, Watermelon w2)
    {
        bool ret = false;
        if (w1.get_seedless() && w2.get_seedless())
        {
            ret = true;
        }
        return ret;
    }
};

int main()
{
    a1.give_info(2.99);
}