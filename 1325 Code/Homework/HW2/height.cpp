/* Parker Skinner
*  ID: 1001541467
*  9/21/2019
*/

#include <sstream>
#include	<iostream>
#include	<vector>
#include	<string>

using namespace std;

class Rider{
  string name;
  int height;
public:
//constructor 1
  Rider(string name, int height){
    this->name=name;
    this->height=height;
  }
//constructor 2
  Rider(int height){
    this->height=height;
    name="";
  }
  int get_height(){
    return height;
  }
};

class Ride{
  string ridename;
  vector<Rider> riders;
  int reqheight;
public:

  void SetVals(int reqheight, string ridename){
    this->reqheight=reqheight;
    this->ridename=ridename;
  }

  void add_line(Rider a){
    if(a.get_height()>=reqheight){
      riders.push_back(a);
      cout << "-Adding rider to line." << endl;
    }
    else{
      cout << "-Sorry can't add rider, too short." << endl;
    }
  }
};

class Amusement_park{
  Ride *rides;
  int numrides;
public:

  Amusement_park(int num){
    numrides=num;
    rides = new Ride[num];

    cout << "~~~ Amusement Park Info ~~~" << endl;
    for (int i=0;i<numrides;i++)
    {
      string input;
      int h;
      string n;
      cout << "Ride " << i+1 << "- Enter minimum ride height and ride name:" << endl;
      getline(cin, input);
      stringstream ss(input);
      ss >> h >> n;
      rides[i].SetVals(h,n);
    }
  }

  Ride get_ride(int num){
    return rides[num-1];
  }
};

int	main()
{
				Rider	r1("Yaris",	45);	//name,	height	in	inches
				Rider	r2(49);	//height	in	inches
				Amusement_park	a1(3);	//3	is	the	number	of	rides	in	the	amusement	park
				a1.get_ride(1).add_line(r1);		//add	a	rider	to	the	line	of	a	ride
				Amusement_park	a2(2);	//2	is	the	number	of	rides	in	the	amusement	park
				a2.get_ride(1).add_line(r2);	//add	a	rider	to	the	line	of	a	ride
				return	0;
}

class example
{
  int a;
  int print();
}

int example::print()
{
  alsdkfjsad
}
