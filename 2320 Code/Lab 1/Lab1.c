/* Parker Skinner
*  ID: 1001541467
*  Compile Command: "gcc Lab1.c"
*
*/
#include <stdio.h>


int binSearchFirst(int *a,int n,int key)
// Input: int array a[] with n elements in ascending order.
//        int key to find.
// Output: Returns subscript of the first a element >= key.
//         Returns n if key>a[n-1].
// Processing: Binary search.
{
  int low,high,mid;
  low=0;
  high=n-1;
// Subscripts between low and high are in search range.
// Size of range halves in each iteration.
// When low>high, low==high+1 and a[high]<key and a[low]>=key.
  while (low<=high)
  {
    mid=(low+high)/2;
    if (a[mid]<key)
      low=mid+1;
    else
      high=mid-1;
  }
  return low;
}

int binSearchLast(int *a,int n,int key)
{
// Input: int array a[] with n elements in ascending order.
//        int key to find.
// Output: Returns subscript of the last a element <= key.
//         Returns -1 if key<a[0].
// Processing: Binary search.
  int low,high,mid;
  low=0;
  high=n-1;
// subscripts between low and high are in search range.
// size of range halves in each iteration.
// When low>high, low==high+1 and a[high]<=key and a[low]>key.
  while (low<=high)
  {
    mid=(low+high)/2;
    if (a[mid]<=key)
      low=mid+1;
    else
      high=mid-1;
  }
  return high;
}

int main(int argc, char **argv[]){
  int n;
  int k;
  printf("How many counters do you want?\n");
  scanf("%d", &n);
  int index[n-1], map[n-1], count[n-1];
  for (k=0;k<n;k++){
    index[k]=k;\
    map[k]=k;
    count[k]=0;
  }
  int check=1;
  int input;
  while(check){
    int i=0;
    int j=0;
    int tempmap=0;
    int tempcount=0;
    int tempindex=0;
    printf("Enter operation number:\n");
    scanf("%d", &input);
    switch (input) {
      case 0:
        check=0;
        return 0;
      case 1:
        for (i=0;i<n;i++){
          printf("%d %d\n",i,count[map[i]]);
          }
        }
        break;
      case 2:
        for (i=0;i<n;i++){
          printf("%d %d\n",index[i],count[i]);
        }
        break;
      case 3:
        printf("Enter Index:\n");
        scanf("%d",&i);
        j=binSearchLast(count,n,count[map[i]]);
        if (j!=-1){
          tempcount=count[map[i]];
          tempindex=index[map[i]];
          tempmap=map[index[i]];
          index[tempmap]=index[map[j]];
          count[tempmap]=count[map[j]];
          map[tempindex]=map[index[j]];
          index[j]=tempindex;
          count[j]=tempcount+1;
          map[index[i]]=tempmap;
        }
        else{
          count[map[i]]+=1;
        }
        break;
      case 4:
      printf("Enter Index:\n");
      scanf("%d",&i);
      j=binSearchFirst(count,n,count[map[i]]);
      if (j!=n){
        tempcount=count[map[i]];
        tempindex=index[map[i]];
        tempmap=map[index[i]];
        index[tempmap]=index[map[j]];
        count[tempmap]=count[map[j]];
        map[tempindex]=map[index[j]];
        index[j]=tempindex;
        count[j]=tempcount-1;
        map[index[i]]=tempmap;
      }
      else{
        count[map[i]]-=1;
      }
        break;
      case 5:
        scanf("%d",&i);
        scanf("%d",&j);
        int k=0;
        int l=0;
        for(k=0;k<n;k++){
          if(count[k]>=i&&count[k]<j){
            l++;
          }
        }
        printf("There are %d counters with values between %d and %d\n",l,i,j );
        break;
      default:
        printf("Incorect Input\n");
        break;
    }
  }
}
