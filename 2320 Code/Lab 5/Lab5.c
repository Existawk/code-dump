/* Parker Skinner
*  ID: 1001541467
*  Compile Command: "gcc Lab5.c"
*
*  Make sure all required files are in
*  the folder when running.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv){
  // IN.DAT FILE OPEN
  FILE *infp;
  infp=fopen("in.dat", "r");
  if(infp==NULL){
    printf("\nin.dat not found.\nExiting......\n");
    exit(0);
  }

  // OUT.DAT FILE INIT
  FILE *outfp;
  outfp=fopen("out.dat","w");

  // IN.DAT FILE READ IN
  char temp[50];
  fscanf(infp,"%s",temp);
  int Filenum=atoi(temp);
  FILE **Files=malloc(sizeof(FILE*)*Filenum);

  // FILE READ INS AND POINTER INIT
  int i=0;
  while(fscanf(infp,"%s",temp)!=EOF){
    FILE *fptemp=fopen(temp,"r");
    Files[i]=fptemp;
    i++;
  }
