/* Parker Skinner
*  ID: 1001541467
*  Compile Command: "gcc Lab2.c"
*
*  Make sure all required files are in
*  the folder when running.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// MINHEAPIFY(Heap, Filenum, Fileindex)
 void minheapify(char **h, int *Fileindex,int Filenum,int i){
    int smallest = i; // Initialize smallest as root
    int l = 2*i + 1; // left = 2*i + 1
    int r = 2*i + 2; // right = 2*i + 2

    // If left child is larger than root
    if (l < Filenum && strcmp(h[l],h[smallest])<0)
        smallest = l;

    // If right child is larger than largest so far
    if (r < Filenum && strcmp(h[r],h[smallest])<0)
        smallest = r;

    // If largest is not root
    if (smallest != i)
    {
        char tempchar[50];
        int tempint=0;
        //swap(arr[i], arr[largest]);
        strcpy(tempchar, h[i]);
        strcpy(h[i], h[smallest]);
        strcpy(h[smallest], tempchar);
        tempint = Fileindex[i];
        Fileindex[i] = Fileindex[smallest];
        Fileindex[smallest] = tempint;
        // Recursively heapify the affected sub-tree
        minheapify(h, Fileindex, Filenum, smallest);
    }
}

void heapsort(char **h, int Filenum, int *Fileindex) {
    // Build heap (rearrange array)
    int i;
    for (i = Filenum / 2 - 1; i >= 0; i--)
        minheapify(h, Fileindex, Filenum, i);

    // One by one extract an element from heap
    for (i=Filenum-1; i>=0; i--)
    {
        // Move current root to end
        //swap(arr[0], arr[i]);
        char tempchar[50];
        int tempint=0;
        strcpy(tempchar, h[i]);
        strcpy(h[i], h[0]);
        strcpy(h[0], tempchar);
        tempint = Fileindex[i];
        Fileindex[i] = Fileindex[0];
        Fileindex[0] = tempint;

        minheapify(h, Fileindex, i, 0);
    }
}
// void minheapify(char **h,int parent, int *Fileindex){
//     char tempchar[50];
//     int tempint=0;
//     int parent_node = (parent-1)/2;
//     int i;
//     if(strcmp(h[parent_node], h[parent])>0){
//         //SWAP AND RECURSIVE CALL
//         strcpy(tempchar, h[parent_node]);
//         strcpy(h[parent_node], h[parent]);
//         strcpy(h[parent], tempchar);
//         tempint = Fileindex[parent_node];
//         Fileindex[parent_node] = Fileindex[parent];
//         Fileindex[parent] = tempint;
//         minheapify(h,parent_node,Fileindex);
//     }
//     for(i=0; i<parent+1;i++){
//       printf("%s\n",h[i] );
//     }
// }


int main(int argc, char **argv){
  // IN.DAT FILE OPEN
  FILE *infp;
  infp=fopen("in.dat", "r");
  if(infp==NULL){
    printf("\nin.dat not found.\nExiting......\n");
    exit(0);
  }

  // OUT.DAT FILE INIT
  FILE *outfp;
  outfp=fopen("out.dat","w");

  // IN.DAT FILE READ IN
  char temp[50];
  fscanf(infp,"%s",temp);
  int Filenum=atoi(temp);
  FILE **Files=malloc(sizeof(FILE*)*Filenum);

  // FILE READ INS AND POINTER INIT
  int i=0;
  while(fscanf(infp,"%s",temp)!=EOF){
    FILE *fptemp=fopen(temp,"r");
    Files[i]=fptemp;
    i++;
  }

  // HEAP INIT
  char **Heap=malloc(Filenum * sizeof(char*));
  int Fileindex[Filenum];
  for(i=0;i<Filenum;i++){
    Heap[i]=malloc(sizeof(char)*50);
    Fileindex[i]=0;
  }

  // HEAP PRIMING
  for(i=0;i<Filenum;i++){
    fscanf(Files[i],"%s",temp);
    strcpy(Heap[i], temp);
    Fileindex[i]=i;
  }
  int Filesremaining=Filenum;
  heapsort(Heap,Filenum-1,Fileindex);


  int Counter=1;
  char previousmin[50];
  int tempindex=Fileindex[0];
  char currentmin[50];
  strcpy(previousmin, Heap[0]);
  fscanf(Files[Fileindex[0]],"%s",Heap[0]);
  heapsort(Heap,Filesremaining-1,Fileindex);

  //HEAP MERGE LOOP
  while(Filesremaining!=0){
    strcpy(currentmin,Heap[0]);

    heapsort(Heap,Filesremaining-1,Fileindex);

    if (strcmp(currentmin,previousmin)==0){
      Counter++;
    }
    else{
      fprintf(outfp, "%s %d\n",Heap[0],Counter);
      Counter=1;
    }

    strcpy(previousmin, Heap[0]);
    if(fscanf(Files[Fileindex[0]],"%s",Heap[0])!=EOF){
      heapsort(Heap,Filesremaining-1,Fileindex);
    }
    else{
      Filesremaining=Filesremaining-1;
      for(i=0;i<Filesremaining;i++){
        Fileindex[i]=Fileindex[i+1];
      }
    }
  }
  fprintf(outfp, "%s %d\n",Heap[0],Counter);

  // FILE POINTER CLOSING AND MALLOC FREEING
  fclose(infp);
  fclose(outfp);
  for(i=0;i<Filenum;i++){
    fclose(Files[i]);
  }
  for(i=0;i<Filenum;i++){
    free(Heap[i]);
  }
  free(Heap);
  free(Files);
}
