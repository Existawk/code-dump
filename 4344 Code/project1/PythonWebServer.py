# Parker Skinner
# ID:1001541467
# 10/15/2021


from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer
from socket import *
import os

class MyServer(BaseHTTPRequestHandler):
     def do_GET(self):

          #inded.html page request
          if self.path == "/index.html":
               fopen = open(os.path.dirname(__file__)+"/index.html").read()
               self.send_response(200)
               
               self.end_headers()
               self.wfile.write(bytes(fopen, 'utf-8'))

          #301 error Handling
          elif self.path == "/home.html":
               self.send_response(301)
               self.send_header("Content-type", "text/html")
               self.end_headers()
               self.wfile.write(bytes("<p>Status Code: %s</p>" % HTTPStatus.MOVED_PERMANENTLY.value, "utf-8"))
               self.wfile.write(bytes("<p>Webpage Moved Permanently to: index.html</p>", "utf-8"))
          
          #404 error Handling
          else:
               self.send_response(404)
               self.send_header("Content-type", "text/html")
               self.end_headers()
               self.wfile.write(bytes("<p>Status Code: %s</p>" % HTTPStatus.NOT_FOUND.value, "utf-8"))
               self.wfile.write(bytes("<p>Page Not Found</p>", "utf-8"))

#Local host and port designation
hostName = "localhost"
serverPort = 8080
WebServer = HTTPServer((hostName, serverPort), MyServer)
print("Server started")
WebServer.serve_forever()
WebServer.server_close()
print("Server stopped.")